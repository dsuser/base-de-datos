<?php

namespace Vehiculos\InventarioBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('VehiculosInventarioBundle:Default:index.html.twig');
    }
}
