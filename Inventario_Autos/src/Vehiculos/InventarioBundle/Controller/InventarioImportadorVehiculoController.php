<?php

namespace Vehiculos\InventarioBundle\Controller;

use Vehiculos\InventarioBundle\Entity\InventarioImportadorVehiculo;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Inventarioimportadorvehiculo controller.
 *
 */
class InventarioImportadorVehiculoController extends Controller
{
    /**
     * Lists all inventarioImportadorVehiculo entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $inventarioImportadorVehiculos = $em->getRepository('VehiculosInventarioBundle:InventarioImportadorVehiculo')->findAll();

        return $this->render('VehiculosTemplateBundle:inventarioimportadorvehiculo:index.html.twig', array(
            'inventarioImportadorVehiculos' => $inventarioImportadorVehiculos,
        ));
    }

    /**
     * Creates a new inventarioImportadorVehiculo entity.
     *
     */
    public function newAction(Request $request)
    {
        $inventarioImportadorVehiculo = new Inventarioimportadorvehiculo();
        $form = $this->createForm('Vehiculos\InventarioBundle\Form\InventarioImportadorVehiculoType', $inventarioImportadorVehiculo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($inventarioImportadorVehiculo);
            $em->flush();

            return $this->redirectToRoute('inventarioimportadorvehiculo_show', array('id' => $inventarioImportadorVehiculo->getId()));
        }

        return $this->render('VehiculosTemplateBundle:inventarioimportadorvehiculo:new.html.twig', array(
            'inventarioImportadorVehiculo' => $inventarioImportadorVehiculo,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a inventarioImportadorVehiculo entity.
     *
     */
    public function showAction(InventarioImportadorVehiculo $inventarioImportadorVehiculo)
    {
        $deleteForm = $this->createDeleteForm($inventarioImportadorVehiculo);

        return $this->render('VehiculosTemplateBundle:inventarioimportadorvehiculo:show.html.twig', array(
            'inventarioImportadorVehiculo' => $inventarioImportadorVehiculo,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing inventarioImportadorVehiculo entity.
     *
     */
    public function editAction(Request $request, InventarioImportadorVehiculo $inventarioImportadorVehiculo)
    {
        $deleteForm = $this->createDeleteForm($inventarioImportadorVehiculo);
        $editForm = $this->createForm('Vehiculos\InventarioBundle\Form\InventarioImportadorVehiculoType', $inventarioImportadorVehiculo);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('inventarioimportadorvehiculo_edit', array('id' => $inventarioImportadorVehiculo->getId()));
        }

        return $this->render('VehiculosTemplateBundle:inventarioimportadorvehiculo:edit.html.twig', array(
            'inventarioImportadorVehiculo' => $inventarioImportadorVehiculo,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a inventarioImportadorVehiculo entity.
     *
     */
    public function deleteAction(Request $request, InventarioImportadorVehiculo $inventarioImportadorVehiculo)
    {
        $form = $this->createDeleteForm($inventarioImportadorVehiculo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($inventarioImportadorVehiculo);
            $em->flush();
        }

        return $this->redirectToRoute('inventarioimportadorvehiculo_index');
    }

    /**
     * Creates a form to delete a inventarioImportadorVehiculo entity.
     *
     * @param InventarioImportadorVehiculo $inventarioImportadorVehiculo The inventarioImportadorVehiculo entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(InventarioImportadorVehiculo $inventarioImportadorVehiculo)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('inventarioimportadorvehiculo_delete', array('id' => $inventarioImportadorVehiculo->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
