<?php

namespace Vehiculos\InventarioBundle\Controller;

use Vehiculos\InventarioBundle\Entity\CatalogoMarca;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Catalogomarca controller.
 *
 */
class CatalogoMarcaController extends Controller
{
    /**
     * Lists all catalogoMarca entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $catalogoMarcas = $em->getRepository('VehiculosInventarioBundle:CatalogoMarca')->findAll();

        return $this->render('VehiculosTemplateBundle:catalogomarca:index.html.twig', array(
            'catalogoMarcas' => $catalogoMarcas,
        ));
    }

    /**
     * Creates a new catalogoMarca entity.
     *
     */
    public function newAction(Request $request)
    {
        $catalogoMarca = new Catalogomarca();
        $form = $this->createForm('Vehiculos\InventarioBundle\Form\CatalogoMarcaType', $catalogoMarca);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($catalogoMarca);
            $em->flush();

            return $this->redirectToRoute('catalogomarca_show', array('id' => $catalogoMarca->getId()));
        }

        return $this->render('VehiculosTemplateBundle:catalogomarca:new.html.twig', array(
            'catalogoMarca' => $catalogoMarca,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a catalogoMarca entity.
     *
     */
    public function showAction(CatalogoMarca $catalogoMarca)
    {
        $deleteForm = $this->createDeleteForm($catalogoMarca);

        return $this->render('VehiculosTemplateBundle:catalogomarca:show.html.twig', array(
            'catalogoMarca' => $catalogoMarca,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing catalogoMarca entity.
     *
     */
    public function editAction(Request $request, CatalogoMarca $catalogoMarca)
    {
        $deleteForm = $this->createDeleteForm($catalogoMarca);
        $editForm = $this->createForm('Vehiculos\InventarioBundle\Form\CatalogoMarcaType', $catalogoMarca);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('catalogomarca_edit', array('id' => $catalogoMarca->getId()));
        }

        return $this->render('VehiculosTemplateBundle:catalogomarca:edit.html.twig', array(
            'catalogoMarca' => $catalogoMarca,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a catalogoMarca entity.
     *
     */
    public function deleteAction(Request $request, CatalogoMarca $catalogoMarca)
    {
        $form = $this->createDeleteForm($catalogoMarca);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($catalogoMarca);
            $em->flush();
        }

        return $this->redirectToRoute('catalogomarca_index');
    }

    /**
     * Creates a form to delete a catalogoMarca entity.
     *
     * @param CatalogoMarca $catalogoMarca The catalogoMarca entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(CatalogoMarca $catalogoMarca)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('catalogomarca_delete', array('id' => $catalogoMarca->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
