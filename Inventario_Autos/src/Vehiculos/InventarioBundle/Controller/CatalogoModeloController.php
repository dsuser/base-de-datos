<?php

namespace Vehiculos\InventarioBundle\Controller;

use Vehiculos\InventarioBundle\Entity\CatalogoModelo;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Catalogomodelo controller.
 *
 */
class CatalogoModeloController extends Controller
{
    /**
     * Lists all catalogoModelo entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $catalogoModelos = $em->getRepository('VehiculosInventarioBundle:CatalogoModelo')->findAll();

        return $this->render('VehiculosTemplateBundle:catalogomodelo:index.html.twig', array(
            'catalogoModelos' => $catalogoModelos,
        ));
    }

    /**
     * Creates a new catalogoModelo entity.
     *
     */
    public function newAction(Request $request)
    {
        $catalogoModelo = new Catalogomodelo();
        $form = $this->createForm('Vehiculos\InventarioBundle\Form\CatalogoModeloType', $catalogoModelo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($catalogoModelo);
            $em->flush();

            return $this->redirectToRoute('catalogomodelo_show', array('id' => $catalogoModelo->getId()));
        }

        return $this->render('VehiculosTemplateBundle:catalogomodelo:new.html.twig', array(
            'catalogoModelo' => $catalogoModelo,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a catalogoModelo entity.
     *
     */
    public function showAction(CatalogoModelo $catalogoModelo)
    {
        $deleteForm = $this->createDeleteForm($catalogoModelo);

        return $this->render('VehiculosTemplateBundle:catalogomodelo:show.html.twig', array(
            'catalogoModelo' => $catalogoModelo,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing catalogoModelo entity.
     *
     */
    public function editAction(Request $request, CatalogoModelo $catalogoModelo)
    {
        $deleteForm = $this->createDeleteForm($catalogoModelo);
        $editForm = $this->createForm('Vehiculos\InventarioBundle\Form\CatalogoModeloType', $catalogoModelo);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('catalogomodelo_edit', array('id' => $catalogoModelo->getId()));
        }

        return $this->render('VehiculosTemplateBundle:catalogomodelo:edit.html.twig', array(
            'catalogoModelo' => $catalogoModelo,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a catalogoModelo entity.
     *
     */
    public function deleteAction(Request $request, CatalogoModelo $catalogoModelo)
    {
        $form = $this->createDeleteForm($catalogoModelo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($catalogoModelo);
            $em->flush();
        }

        return $this->redirectToRoute('catalogomodelo_index');
    }

    /**
     * Creates a form to delete a catalogoModelo entity.
     *
     * @param CatalogoModelo $catalogoModelo The catalogoModelo entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(CatalogoModelo $catalogoModelo)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('catalogomodelo_delete', array('id' => $catalogoModelo->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
