<?php

namespace Vehiculos\InventarioBundle\Controller;

use Vehiculos\InventarioBundle\Entity\CatalogoGenero;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Catalogogenero controller.
 *
 */
class CatalogoGeneroController extends Controller
{
    /**
     * Lists all catalogoGenero entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $catalogoGeneros = $em->getRepository('VehiculosInventarioBundle:CatalogoGenero')->findAll();

        return $this->render('VehiculosTemplateBundle:catalogogenero:index.html.twig', array(
            'catalogoGeneros' => $catalogoGeneros,
        ));
    }

    /**
     * Creates a new catalogoGenero entity.
     *
     */
    public function newAction(Request $request)
    {
        $catalogoGenero = new Catalogogenero();
        $form = $this->createForm('Vehiculos\InventarioBundle\Form\CatalogoGeneroType', $catalogoGenero);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($catalogoGenero);
            $em->flush();

            return $this->redirectToRoute('catalogogenero_show', array('id' => $catalogoGenero->getId()));
        }

        return $this->render('VehiculosTemplateBundle:catalogogenero:new.html.twig', array(
            'catalogoGenero' => $catalogoGenero,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a catalogoGenero entity.
     *
     */
    public function showAction(CatalogoGenero $catalogoGenero)
    {
        $deleteForm = $this->createDeleteForm($catalogoGenero);

        return $this->render('VehiculosTemplateBundle:catalogogenero:show.html.twig', array(
            'catalogoGenero' => $catalogoGenero,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing catalogoGenero entity.
     *
     */
    public function editAction(Request $request, CatalogoGenero $catalogoGenero)
    {
        $deleteForm = $this->createDeleteForm($catalogoGenero);
        $editForm = $this->createForm('Vehiculos\InventarioBundle\Form\CatalogoGeneroType', $catalogoGenero);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('catalogogenero_edit', array('id' => $catalogoGenero->getId()));
        }

        return $this->render('VehiculosTemplateBundle:catalogogenero:edit.html.twig', array(
            'catalogoGenero' => $catalogoGenero,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a catalogoGenero entity.
     *
     */
    public function deleteAction(Request $request, CatalogoGenero $catalogoGenero)
    {
        $form = $this->createDeleteForm($catalogoGenero);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($catalogoGenero);
            $em->flush();
        }

        return $this->redirectToRoute('catalogogenero_index');
    }

    /**
     * Creates a form to delete a catalogoGenero entity.
     *
     * @param CatalogoGenero $catalogoGenero The catalogoGenero entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(CatalogoGenero $catalogoGenero)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('catalogogenero_delete', array('id' => $catalogoGenero->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
