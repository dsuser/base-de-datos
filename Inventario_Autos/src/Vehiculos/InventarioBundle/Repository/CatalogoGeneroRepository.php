<?php

namespace Vehiculos\InventarioBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * CatalogoGeneroRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class CatalogoGeneroRepository extends EntityRepository
{
}
