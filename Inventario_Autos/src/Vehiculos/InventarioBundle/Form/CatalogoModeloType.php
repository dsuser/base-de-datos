<?php

namespace Vehiculos\InventarioBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vehiculos\InventarioBundle\Entity\CatalogoMarca;
class CatalogoModeloType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('marca',
                'entity', 
                    array
                    (
                        'class'=> CatalogoMarca::class,
                        'choice_label' => 'nombreMarca',
                        'placeholder' => 'Seleccione la marca del modelo a ingresar',
                        'attr' => array
                        (
                            'class' => 'form_control'
                        )
                    )
            )
        ->add('nombreModelo');

    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Vehiculos\InventarioBundle\Entity\CatalogoModelo'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'vehiculos_inventariobundle_catalogomodelo';
    }

}
