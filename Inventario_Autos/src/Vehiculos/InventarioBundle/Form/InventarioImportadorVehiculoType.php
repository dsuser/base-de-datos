<?php

namespace Vehiculos\InventarioBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vehiculos\InventarioBundle\Entity\CatalogoGenero;

class InventarioImportadorVehiculoType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('nombreImportador')
        ->add('apellidoImportador')
        ->add('fechaNacimientoImportador')
        ->add('direccionImportador')
        ->add('telefonoCelularImportador')
        ->add('telefonoFijoImportador')
        ->add('personaResponsableImportador')
        ->add('correoElectronicoImportador')
        ->add('apellidoCasadaImportador')
        ->add('nitimportador')
        ->add('genero',
                'entity', array
                (
                    'class' => CatalogoGenero::class,
                    'choice_label' => 'nombreGenero',
                    'attr' => array
                    (
                        'class' => 'form_control'
                    )
                )
            );
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Vehiculos\InventarioBundle\Entity\InventarioImportadorVehiculo'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'vehiculos_inventariobundle_inventarioimportadorvehiculo';
    }


}
