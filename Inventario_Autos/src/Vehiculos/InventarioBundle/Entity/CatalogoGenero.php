<?php

namespace Vehiculos\InventarioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CatalogoGenero
 *
 * @ORM\Table(name="catalogo_genero")
 * @ORM\Entity
 */
class CatalogoGenero
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="catalogo_genero_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre_genero", type="string", length=50, nullable=false)
     */
    private $nombreGenero;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion_genero", type="string", length=255, nullable=false)
     */
    private $descripcionGenero;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombreGenero
     *
     * @param string $nombreGenero
     *
     * @return CatalogoGenero
     */
    public function setNombreGenero($nombreGenero)
    {
        $this->nombreGenero = $nombreGenero;

        return $this;
    }

    /**
     * Get nombreGenero
     *
     * @return string
     */
    public function getNombreGenero()
    {
        return $this->nombreGenero;
    }

    /**
     * Set descripcionGenero
     *
     * @param string $descripcionGenero
     *
     * @return CatalogoGenero
     */
    public function setDescripcionGenero($descripcionGenero)
    {
        $this->descripcionGenero = $descripcionGenero;

        return $this;
    }

    /**
     * Get descripcionGenero
     *
     * @return string
     */
    public function getDescripcionGenero()
    {
        return $this->descripcionGenero;
    }
}
