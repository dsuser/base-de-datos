<?php

namespace Vehiculos\InventarioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CatalogoModelo
 *
 * @ORM\Table(name="catalogo_modelo", indexes={@ORM\Index(name="fki_modelo_marca_fk", columns={"marca_id"})})
 * @ORM\Entity
 */
class CatalogoModelo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="catalogo_modelo_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre_modelo", type="string", length=75, nullable=false)
     */
    private $nombreModelo;

    /**
     * @var \CatalogoMarca
     *
     * @ORM\ManyToOne(targetEntity="CatalogoMarca")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="marca_id", referencedColumnName="id")
     * })
     */
    private $marca;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombreModelo
     *
     * @param string $nombreModelo
     *
     * @return CatalogoModelo
     */
    public function setNombreModelo($nombreModelo)
    {
        $this->nombreModelo = $nombreModelo;

        return $this;
    }

    /**
     * Get nombreModelo
     *
     * @return string
     */
    public function getNombreModelo()
    {
        return $this->nombreModelo;
    }

    /**
     * Set marca
     *
     * @param \Vehiculos\InventarioBundle\Entity\CatalogoMarca $marca
     *
     * @return CatalogoModelo
     */
    public function setMarca(\Vehiculos\InventarioBundle\Entity\CatalogoMarca $marca = null)
    {
        $this->marca = $marca;

        return $this;
    }

    /**
     * Get marca
     *
     * @return \Vehiculos\InventarioBundle\Entity\CatalogoMarca
     */
    public function getMarca()
    {
        return $this->marca;
    }
}
