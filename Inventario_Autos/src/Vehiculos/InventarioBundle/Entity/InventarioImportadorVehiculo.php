<?php

namespace Vehiculos\InventarioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InventarioImportadorVehiculo
 *
 * @ORM\Table(name="inventario_importador_vehiculo", indexes={@ORM\Index(name="fki_importador_genero_fk", columns={"genero_id"})})
 * @ORM\Entity
 */
class InventarioImportadorVehiculo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="inventario_importador_vehiculo_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre_importador", type="string", length=70, nullable=false)
     */
    private $nombreImportador;

    /**
     * @var string
     *
     * @ORM\Column(name="apellido_importador", type="string", length=70, nullable=false)
     */
    private $apellidoImportador;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_nacimiento_importador", type="datetime", nullable=false)
     */
    private $fechaNacimientoImportador;

    /**
     * @var string
     *
     * @ORM\Column(name="direccion_importador", type="string", length=255, nullable=false)
     */
    private $direccionImportador;

    /**
     * @var string
     *
     * @ORM\Column(name="telefono_celular_importador", type="string", length=25, nullable=false)
     */
    private $telefonoCelularImportador;

    /**
     * @var string
     *
     * @ORM\Column(name="telefono_fijo_importador", type="string", length=25, nullable=false)
     */
    private $telefonoFijoImportador;

    /**
     * @var string
     *
     * @ORM\Column(name="persona_responsable_importador", type="string", length=140, nullable=false)
     */
    private $personaResponsableImportador;

    /**
     * @var string
     *
     * @ORM\Column(name="correo_electronico_importador", type="string", length=100, nullable=false)
     */
    private $correoElectronicoImportador;

    /**
     * @var string
     *
     * @ORM\Column(name="apellido_casada_importador", type="string", length=70, nullable=true)
     */
    private $apellidoCasadaImportador;

    /**
     * @var string
     *
     * @ORM\Column(name="nitimportador", type="string", length=25, nullable=false)
     */
    private $nitimportador;

    /**
     * @var \CatalogoGenero
     *
     * @ORM\ManyToOne(targetEntity="CatalogoGenero")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="genero_id", referencedColumnName="id")
     * })
     */
    private $genero;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombreImportador
     *
     * @param string $nombreImportador
     *
     * @return InventarioImportadorVehiculo
     */
    public function setNombreImportador($nombreImportador)
    {
        $this->nombreImportador = $nombreImportador;

        return $this;
    }

    /**
     * Get nombreImportador
     *
     * @return string
     */
    public function getNombreImportador()
    {
        return $this->nombreImportador;
    }

    /**
     * Set apellidoImportador
     *
     * @param string $apellidoImportador
     *
     * @return InventarioImportadorVehiculo
     */
    public function setApellidoImportador($apellidoImportador)
    {
        $this->apellidoImportador = $apellidoImportador;

        return $this;
    }

    /**
     * Get apellidoImportador
     *
     * @return string
     */
    public function getApellidoImportador()
    {
        return $this->apellidoImportador;
    }

    /**
     * Set fechaNacimientoImportador
     *
     * @param \DateTime $fechaNacimientoImportador
     *
     * @return InventarioImportadorVehiculo
     */
    public function setFechaNacimientoImportador($fechaNacimientoImportador)
    {
        $this->fechaNacimientoImportador = $fechaNacimientoImportador;

        return $this;
    }

    /**
     * Get fechaNacimientoImportador
     *
     * @return \DateTime
     */
    public function getFechaNacimientoImportador()
    {
        return $this->fechaNacimientoImportador;
    }

    /**
     * Set direccionImportador
     *
     * @param string $direccionImportador
     *
     * @return InventarioImportadorVehiculo
     */
    public function setDireccionImportador($direccionImportador)
    {
        $this->direccionImportador = $direccionImportador;

        return $this;
    }

    /**
     * Get direccionImportador
     *
     * @return string
     */
    public function getDireccionImportador()
    {
        return $this->direccionImportador;
    }

    /**
     * Set telefonoCelularImportador
     *
     * @param string $telefonoCelularImportador
     *
     * @return InventarioImportadorVehiculo
     */
    public function setTelefonoCelularImportador($telefonoCelularImportador)
    {
        $this->telefonoCelularImportador = $telefonoCelularImportador;

        return $this;
    }

    /**
     * Get telefonoCelularImportador
     *
     * @return string
     */
    public function getTelefonoCelularImportador()
    {
        return $this->telefonoCelularImportador;
    }

    /**
     * Set telefonoFijoImportador
     *
     * @param string $telefonoFijoImportador
     *
     * @return InventarioImportadorVehiculo
     */
    public function setTelefonoFijoImportador($telefonoFijoImportador)
    {
        $this->telefonoFijoImportador = $telefonoFijoImportador;

        return $this;
    }

    /**
     * Get telefonoFijoImportador
     *
     * @return string
     */
    public function getTelefonoFijoImportador()
    {
        return $this->telefonoFijoImportador;
    }

    /**
     * Set personaResponsableImportador
     *
     * @param string $personaResponsableImportador
     *
     * @return InventarioImportadorVehiculo
     */
    public function setPersonaResponsableImportador($personaResponsableImportador)
    {
        $this->personaResponsableImportador = $personaResponsableImportador;

        return $this;
    }

    /**
     * Get personaResponsableImportador
     *
     * @return string
     */
    public function getPersonaResponsableImportador()
    {
        return $this->personaResponsableImportador;
    }

    /**
     * Set correoElectronicoImportador
     *
     * @param string $correoElectronicoImportador
     *
     * @return InventarioImportadorVehiculo
     */
    public function setCorreoElectronicoImportador($correoElectronicoImportador)
    {
        $this->correoElectronicoImportador = $correoElectronicoImportador;

        return $this;
    }

    /**
     * Get correoElectronicoImportador
     *
     * @return string
     */
    public function getCorreoElectronicoImportador()
    {
        return $this->correoElectronicoImportador;
    }

    /**
     * Set apellidoCasadaImportador
     *
     * @param string $apellidoCasadaImportador
     *
     * @return InventarioImportadorVehiculo
     */
    public function setApellidoCasadaImportador($apellidoCasadaImportador)
    {
        $this->apellidoCasadaImportador = $apellidoCasadaImportador;

        return $this;
    }

    /**
     * Get apellidoCasadaImportador
     *
     * @return string
     */
    public function getApellidoCasadaImportador()
    {
        return $this->apellidoCasadaImportador;
    }

    /**
     * Set nitimportador
     *
     * @param string $nitimportador
     *
     * @return InventarioImportadorVehiculo
     */
    public function setNitimportador($nitimportador)
    {
        $this->nitimportador = $nitimportador;

        return $this;
    }

    /**
     * Get nitimportador
     *
     * @return string
     */
    public function getNitimportador()
    {
        return $this->nitimportador;
    }

    /**
     * Set genero
     *
     * @param \Vehiculos\InventarioBundle\Entity\CatalogoGenero $genero
     *
     * @return InventarioImportadorVehiculo
     */
    public function setGenero(\Vehiculos\InventarioBundle\Entity\CatalogoGenero $genero = null)
    {
        $this->genero = $genero;

        return $this;
    }

    /**
     * Get genero
     *
     * @return \Vehiculos\InventarioBundle\Entity\CatalogoGenero
     */
    public function getGenero()
    {
        return $this->genero;
    }
}
