<?php

namespace Vehiculos\InventarioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InventarioDesperfectoImportacion
 *
 * @ORM\Table(name="inventario_desperfecto_importacion", indexes={@ORM\Index(name="fki_desperfecto_importacion_fk", columns={"importacion_id"})})
 * @ORM\Entity
 */
class InventarioDesperfectoImportacion
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="inventario_desperfecto_importacion_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion_desperfecto", type="string", length=255, nullable=false)
     */
    private $descripcionDesperfecto;

    /**
     * @var string
     *
     * @ORM\Column(name="fotografia_desperfecto", type="string", length=255, nullable=false)
     */
    private $fotografiaDesperfecto;

    /**
     * @var \InventarioImportacionRealizada
     *
     * @ORM\ManyToOne(targetEntity="InventarioImportacionRealizada")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="importacion_id", referencedColumnName="id")
     * })
     */
    private $importacion;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descripcionDesperfecto
     *
     * @param string $descripcionDesperfecto
     *
     * @return InventarioDesperfectoImportacion
     */
    public function setDescripcionDesperfecto($descripcionDesperfecto)
    {
        $this->descripcionDesperfecto = $descripcionDesperfecto;

        return $this;
    }

    /**
     * Get descripcionDesperfecto
     *
     * @return string
     */
    public function getDescripcionDesperfecto()
    {
        return $this->descripcionDesperfecto;
    }

    /**
     * Set fotografiaDesperfecto
     *
     * @param string $fotografiaDesperfecto
     *
     * @return InventarioDesperfectoImportacion
     */
    public function setFotografiaDesperfecto($fotografiaDesperfecto)
    {
        $this->fotografiaDesperfecto = $fotografiaDesperfecto;

        return $this;
    }

    /**
     * Get fotografiaDesperfecto
     *
     * @return string
     */
    public function getFotografiaDesperfecto()
    {
        return $this->fotografiaDesperfecto;
    }

    /**
     * Set importacion
     *
     * @param \Vehiculos\InventarioBundle\Entity\InventarioImportacionRealizada $importacion
     *
     * @return InventarioDesperfectoImportacion
     */
    public function setImportacion(\Vehiculos\InventarioBundle\Entity\InventarioImportacionRealizada $importacion = null)
    {
        $this->importacion = $importacion;

        return $this;
    }

    /**
     * Get importacion
     *
     * @return \Vehiculos\InventarioBundle\Entity\InventarioImportacionRealizada
     */
    public function getImportacion()
    {
        return $this->importacion;
    }
}
