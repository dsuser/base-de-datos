<?php

namespace Vehiculos\InventarioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InventarioImportacionRealizada
 *
 * @ORM\Table(name="inventario_importacion_realizada", indexes={@ORM\Index(name="fki_importacion_marca_fk", columns={"marca_id"}), @ORM\Index(name="fki_importacion_realizada_modelo_fk", columns={"modelo_id"})})
 * @ORM\Entity
 */
class InventarioImportacionRealizada
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="inventario_importacion_realizada_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="datetime", nullable=false)
     */
    private $fecha;

    /**
     * @var string
     *
     * @ORM\Column(name="anio", type="string", length=10, nullable=false)
     */
    private $anio;

    /**
     * @var \CatalogoMarca
     *
     * @ORM\ManyToOne(targetEntity="CatalogoMarca")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="marca_id", referencedColumnName="id")
     * })
     */
    private $marca;

    /**
     * @var \CatalogoModelo
     *
     * @ORM\ManyToOne(targetEntity="CatalogoModelo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="modelo_id", referencedColumnName="id")
     * })
     */
    private $modelo;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     *
     * @return InventarioImportacionRealizada
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set anio
     *
     * @param string $anio
     *
     * @return InventarioImportacionRealizada
     */
    public function setAnio($anio)
    {
        $this->anio = $anio;

        return $this;
    }

    /**
     * Get anio
     *
     * @return string
     */
    public function getAnio()
    {
        return $this->anio;
    }

    /**
     * Set marca
     *
     * @param \Vehiculos\InventarioBundle\Entity\CatalogoMarca $marca
     *
     * @return InventarioImportacionRealizada
     */
    public function setMarca(\Vehiculos\InventarioBundle\Entity\CatalogoMarca $marca = null)
    {
        $this->marca = $marca;

        return $this;
    }

    /**
     * Get marca
     *
     * @return \Vehiculos\InventarioBundle\Entity\CatalogoMarca
     */
    public function getMarca()
    {
        return $this->marca;
    }

    /**
     * Set modelo
     *
     * @param \Vehiculos\InventarioBundle\Entity\CatalogoModelo $modelo
     *
     * @return InventarioImportacionRealizada
     */
    public function setModelo(\Vehiculos\InventarioBundle\Entity\CatalogoModelo $modelo = null)
    {
        $this->modelo = $modelo;

        return $this;
    }

    /**
     * Get modelo
     *
     * @return \Vehiculos\InventarioBundle\Entity\CatalogoModelo
     */
    public function getModelo()
    {
        return $this->modelo;
    }
}
