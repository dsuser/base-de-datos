<?php

namespace Vehiculos\TemplateBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('VehiculosTemplateBundle:Default:index.html.twig');
    }
}
